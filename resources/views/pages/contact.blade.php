@extends('index')

@section('content')

<div class="contact-page">
    <section class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-95 s-o-padding-b-30 m-o-padding-t-130 m-o-padding-b-50">
        <div class="row">
            <div class="columns small-12 large-4">
                <div class="section-header margin-b-25">
                    <h2 class="no-margin darkBrown-color font-italics font-serif font-weight-600">
                        Contact Details
                    </h2>
                </div>

                <div class="contactAddress">
                    <h6 class="darkPink-color font-lato font-weight-600 margin-b-30">
                        321 BHOOMI MALL,<br/> 
                        SECTOR-15, PALM BEACH ROAD,<br/>
                        CBD BELAPUR, NAVI MUMBAI-400614. 
                    </h6>

                    <a href="mailto:info@paawakfoods.com" class="darkPink-color font-lato">info@paawakfoods.com</a>
                    <!-- <a class="darkPink-color font-lato margin-b-30">kaushaladongre@gmail.com</a> -->

                    <h6 class="darkPink-color font-lato">
                        +91 982-101-8524 
                    </h6>
                </div>
            </div>
            <div class="columns small-12 large-8">
                <div class="section-header margin-b-25 s-o-margin-tb-25 m-o-margin-t-30">
                    <h2 class="darkBrown-color no-margin font-italics font-serif font-weight-600">
                        Contact Form
                    </h2>
                </div>
                <div class="contactForm">
                    <form data-abide id="contactpage__form" action="{{route('contact.post')}}">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
                        <input type="hidden" value="contactpage__form" name="contactpage__form__id"/>
                        
                        <div class="row">
                            <div class="columns small-12 large-4">
                                <input class="bgColor-wheat" name="name" type="text" placeholder="Name *" required pattern="[a-zA-Z]+"/>
                                <small class="error bgColor-black">Name is required and must be a string.</small>
                            </div>
                            <div class="columns small-12 large-4">
                                <input class="bgColor-wheat" name="email" type="email" placeholder="Email *" required/>
                                <small class="error bgColor-black">An email address is required.</small>
                            </div>
                            <div class="columns small-12 large-4">
                                <input class="bgColor-wheat" name="tel" type="tel" placeholder="Phone"/>
                            </div>
                            <div class="columns small-12 large-12">
                                <textarea class="bgColor-wheat" name="message" placeholder="Message"></textarea>
                            </div>

                            <div class="columns small-6 large-3">
                                <button type="reset" class="bgColor-darkPink clear-btn whiteColor text-center no-margin">CLEAR</button>
                            </div>
                            <div class="columns small-6 large-3 end">
                                <button class="bgColor-darkPink send-btn whiteColor text-center no-margin">SEND</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns large-12 small-12 medium-12">
                <!-- <div class="location-map"> -->
                    <div id="map"></div>
                <!-- </div> -->
            </div>
        </div>
    </section>
</div>
@endsection