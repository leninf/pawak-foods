@extends('index')

@section('content')

    <div class="home-page">
        <section class="section-1 banner-img">

            <div class="banner parent-table">
                {{--<img src="../img/paawak-banner-image.jpg">--}}
                <div class="overlay"></div>
                <div class="banner-tagline child-table-cell">

                    <div class="row">
                        <div class="columns large-12 small-12 medium-12">
                            <h1 class="no-margin custom-lh default-font-family">
                                We have best quality <br/>
                                fresh jaggery
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-2 padding-tb-50 s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns large-8 medium-12 small-12">
                    <div class="welcome-section">
                        <h2 class="no-margin default-title-style default-font-family small-text-center-only">Welcome to Paawak Foods!</h2>

                        <p class="no-margin small-text-center-only">
                            Paawak Foods is a fast growing company engaged in agro products. Since inception the
                            company’s core business has been crafting a distinct identity by developing its own brand in 
                            jaggery and jaggery based products. The quality of our jaggery products is optimal as they are procured
                            from the best sources. We process them under hygienic conditions by adopting quality
                            methods. We conduct strict quality check tests before delivering the products to our clients
                            to ensure that no substandard product is delivered to the client.
                        </p>
                    </div>
                </div>

                <div class="columns large-4 medium-12 small-12 text-center">
                    <div class="button-content">
                        <div class="wrapper">
                            <a href="{{route('about')}}"
                               class="bgColor-darkPink about-btn whiteColor text-center">ABOUT US</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-3 padding-tb-50 bgColor-wheat s-o-padding-tb-40 m-o-padding-tb-50 default-font-color">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="no-margin default-font-family default-title-style">
                            Our Products
                        </h2>
                    </div>
                </div>
                <div class="columns small-12">
                    <div class="description">
                    We have carved a niche for ourselves in the arena of manufacturing & marketing of comprehensive range of natural jaggery & jaggery based products.<br/><br/>
                        As per research, jaggery is not given the first preference as natural sweetener because of the inconsistent quality and inconvenient size of jaggery. Also jaggery available in the current market has a lot of harmful chemicals. Hence to overcome these issues, we have introduced natural chemical free jaggery in small cubes and powdered form. <br/><br/>
                        ‘Purifying Yourself’ is the mantra of our ‘Homa Therapy Agnihotra' jaggery powder. 
                        The central idea of Homa Therapy is, you heal the atmosphere and the healed atmosphere heals you. 
                        Paawak natural jaggery powder is made from specially grown sugar cane which is cultivated as per 
                        Homa and Agnihotra process. <br/><br/>
                        Our natural jaggery is available in two variants:
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns large-6 small-12 medium-6 text-center">
                    <div class="product-display-section">
                        <a href="{{route('lump_products')}}" class="inline-block link-hover-effect">
                            <img src="../img/home/o_j_l.jpg">

                            <h4 class="font-weight-600 margin-t20 no-margin-b default-font-color s-o-margin-b-30">NATURAL JAGGERY CUBES</h4>
                        </a>
                    </div>
                </div>

                <div class="columns large-6 small-12 medium-6 text-center">
                    <div class="product-display-section">
                        <a href="{{route('powdered_products')}}" class="inline-block link-hover-effect">
                            <img src="../img/home/o_j_p.jpg">

                            <h4 class="font-weight-600 margin-t20 no-margin-b default-font-color">NATURAL JAGGERY
                                POWDER</h4>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-4 padding-tb-50 s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12 large-12 medium-12">
                    <div class="product-content">
                        <h2 class="default-font-family default-title-style margin-b-only">
                            <i class="default-font-family default-title-style">
                                Why Paawak?
                            </i>
                        </h2>

                        <div class="table-content">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Paawak means 'purifying'. We innovate and provide products which signify what we stand for!
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Prime reason to start Paawak is to provide products with a main focus on the most essential three things - quality, quality and quality!
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        We emphasize on customer satisfaction, which is our ultimate aim!
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Our priority is to provide only nutritional products and stay aligned to the vision and mission of Paawak!
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-5 padding-tb-50 bgColor-wheat s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="no-margin default-title-style default-font-family">
                            What Our Clients Say About Us
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row height-equalizer-wrapper">
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                Paawak Jaggery is the most naturally made and tastes very good. 
                                Its original sugarcane taste is what makes it very distinct. 
                                In the world where there is food adulteration in almost every thing, 
                                it's good to know that brands like Paawak Jaggery are making such 
                                healthy and delicious products.</q></p>

                        <cite>Dinesh Solanki</cite>
                    </blockquote>
                </div>
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                Paawak organic jaggery is good for our health and it prevents constipation, 
                                helps to build haemoglobin, increases immunity. Treats flu like symptoms like 
                                cough - cold, migraine. We can use Paawak organic jaggery to make laddus and mithai.</q></p>

                        <cite>Ujwala Sancheti</cite>
                    </blockquote>
                </div>
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                I love the brand Paawak because I use it for my kid. 
                                It suits him that's the best part. I love Paawak specially cause of the customer service. 
                                They are very prompt and efficient. Please keep up the good work.</q></p>

                        <cite>Aneesha Prakash</cite>
                    </blockquote>
                </div>
            </div>
        </section>
    </div>


@endsection