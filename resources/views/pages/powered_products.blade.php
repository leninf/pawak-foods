@extends('index')

@section('content')
    <div class="product-page">
        <section
                class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-100 s-o-padding-b-30 m-o-padding-t-90 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12">
                    <div class="page-title large-text-center small-text-left default-font-family">
                        Powdered Jaggery
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns large-8 small-12 medium-8">

                    <div class="product-slider">

                        <div class="slide">
                            <div class="image">
                                <img src="../img/product/pro2.jpg">
                            </div>
                        </div>
                    </div>

                    <div class="product-title margin-tb-20">
                        <h4 class="darkBrown-color font-weight-600 custom-lh">
                            Powdered Jaggery
                        </h4>
                    </div>

                    <div class="product-desc">
                        <p class="darkBrown-color">
                        The taste, free-flowingness and nutritional value is what sets Paawak jaggery powder apart from other jaggery powders.  This powder is produced from sugarcane cultivated organically with special emphasis on Agnihotra Homa farming. By putting certain materials such as medicinal herbs into specially prepared fires accompanied by chanting of certain mantras, it is possible to remove toxic conditions from atmosphere, thereby healing and purifying it. The healing effect is then passed on to all life forms which are sustained by that atmosphere. Our powder is a beneficiary of this wonderful process.
                        </p>
                    </div>
                </div>
                <div class="columns large-4 small-12 medium-4">
                    <!-- <div class="section-header margin-b-only">
                        <h2 class="darkBrown-color no-margin font-italics font-serif font-weight-600">
                           Points
                        </h2>
                    </div> -->

                    <div class="points-list">

                        <table>
                            <tbody>
                            @foreach($points as $index => $point)
                                <tr>
                                    <td>
                                        <i class="fa fa-arrow-circle-right"></i>

                                    </td>
                                    <td>
                                        <span class="margin-l-15">{{$point}}</span>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()