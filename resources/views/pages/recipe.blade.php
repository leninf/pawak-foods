@extends('index')

@section('content')

<div class="recipe-page">
    <section class="section-1 bgColor-wheat padding-tb-100 s-o-padding-t-100 s-o-padding-b-30 m-o-padding-t-90 m-o-padding-b-50">
        <div class="row">
            <div class="columns large-8 small-12 medium-12">

                <div class="recipe-img">
                    <img src="../img/gallery/{{$recipe["recipe_image"]}}" alt=""/>
                </div>

                <div class="recipe-title margin-tb-20">
                    <h4 class="darkBrown-color font-weight-600 custom-lh">
                        {{ $recipe["recipe_name"] }}
                    </h4>
                </div>

                <div class="recipe-desc">
                    <p class="darkBrown-color">
                        {{ $recipe["recipe_desc"] }}
                    </p>
                </div>
            </div>

            <div class="columns large-4 small-12 medium-12">
                <div class="section-header margin-b-only">
                    <h2 class="darkBrown-color no-margin font-italics font-serif font-weight-600">
                        Ingredients
                    </h2>
                </div>

                <div class="ingredient-list">
                    <ul class="menu no-margin">

                        @foreach($recipe['ingredients'] as $key => $value)
                            <li class="menu-items margin-b-only">
                                <div class="darkBrown-color">
                                    <i class="fa fa-arrow-circle-right"></i>
                                    <span class="margin-l-15">{{$value["name"]}} - {{$value["qty"]}}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection