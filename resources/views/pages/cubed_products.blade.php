@extends('index')

@section('content')
    <div class="product-page">
        <section
                class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-100 s-o-padding-b-30 m-o-padding-t-90 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12">
                    <div class="page-title large-text-center small-text-left default-font-family">
                        Jaggery Cubes
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns large-8 small-12 medium-8">

                    <div class="product-slider">

                        <div class="slide">
                            <div class="image">
                                <img src="../img/product/pro1.jpg">
                            </div>
                        </div>
                    </div>

                    <div class="product-title margin-tb-20">
                        <h4 class="darkBrown-color font-weight-600 custom-lh">
                            Jaggery Cubes
                        </h4>
                    </div>

                    <div class="product-desc">
                        <p class="darkBrown-color">
                            This is an innovative product designed to meet your daily need of energy levels. This product is an outcome of extensive consumer research and technological solution applied to traditional method of jaggery making. Product is designed and manufactured with high level of hygiene and in an automated plant, so you always get a high quality product. Process has been  changed to give you the best natural product which is free of any adulteration, impurities and harmful chemicals.
                        </p>
                        <p>
                            We are the first in the country to introduce jaggery in this innovative and convenient cube size.
                        </p>
                    </div>
                </div>
                <div class="columns large-4 small-12 medium-4">
                    <!-- <div class="section-header margin-b-only">
                        <h2 class="darkBrown-color no-margin font-italics font-serif font-weight-600">
                            Points
                        </h2>
                    </div> -->

                    <div class="points-list">
                        <table>
                            <tbody>
                            @foreach($points as $index => $point)
                                <tr>
                                    <td>
                                        <i class="fa fa-arrow-circle-right"></i>

                                    </td>
                                    <td>
                                        <span class="margin-l-15">{{$point}}</span>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()