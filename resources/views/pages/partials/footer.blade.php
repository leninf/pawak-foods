

<footer class="bgColor-white footer-section small-only-text-center">
    <div class="row padding-t-50 padding-b-30 s-o-padding-tb-30 border-b-1 m-o-padding-tb-50">
        <div class="columns large-6 large-centered medium-8 medium-centered small-8 small-centered">
            <div class="row">
                <div class="columns large-4 small-4 text-center">
                    <a class="social-icons bgColor-darkBrown">
                        <i class="fa fa-facebook"></i>
                    </a>
                </div>
                <div class="columns large-4 small-4 text-center">
                    <a class="social-icons bgColor-darkBrown">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
                <div class="columns large-4 small-4 text-center">
                    <a class="social-icons bgColor-darkBrown">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-t-30 padding-b-30 s-o-padding-tb-30 m-o-padding-tb-30">
        <div class="columns large-6 small-12 large-centered">
            <div class="copyright text-center">
                Paawak Foods © 2015 - All Rights Reserved <br/>
                Powered By <a class="dark-yellow" href="http://www.speakinglamp.com">SpeakingLamp</a>
            </div>
        </div>
    </div>
</footer>