<div class="pre-header bgColor-wheat padding-tb-20 show-for-large-up">
    <div class="row">
        <div class="columns large-4 medium-12 small-12">
            <a href="{{route('home')}}" class="brand-logo inline-block">
                <img src="/img/icon.png" alt=""/>
            </a>
        </div>

        <div class="columns large-8 medium-6 small-12">
            <div class="brand-info right">
                <div class="fa fa-phone border-right padding-t-15">
                    <p class="fs-dot75-rem margin-b-5">Phone:</p>
                    <span class="fs-dot75-rem">+91 982-101-8524</span>
                    <!-- <span class="fs-dot75-rem">+91 9012345678</span> -->
                    <div>&nbsp</div>
                </div>
                <div class="fa fa-map-marker no-margin">
                    <p class="fs-dot75-rem margin-b-5">Location:</p>

                    <p class="fs-dot75-rem no-margin custom-lh">321 Bhoomi Mall,<br/>
                        Sector-15 CBD Belapur,Navi-Mumbai.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<header class="pawak-header hide-for-small-only hide-for-medium-only bgColor-yellow">
    <div class="header-wrapper">
        <div class="row">
            <div class="columns large-12">
                <nav>
                    <div class="left navigation-bar">
                        <ul class="no-margin nav-menu">
                            <li class="navMenu-items">
                                <a class="whiteColor font-weight-600 {{Route::currentRouteName() == 'home' ? 'top-header-li-active' : ''}}"
                                   href="{{route('home')}}">HOME</a>
                            </li>
                            <li class="navMenu-items" id="products-menu">
                                <a class="has-drop-down whiteColor font-weight-600">PRODUCTS</a>
                                <ul class="product-menu-lists">
                                    <li class="product-menu-item navMenu-items {{Route::currentRouteName() == 'lump_products' ? 'top-header-li-active' : ''}}">
                                        <a href="{{route('lump_products')}}"
                                           class="whiteColor font-weight-600 no-margin">JAGGERY CUBES</a></li>
                                    <li class="border-t-wheat product-menu-item navMenu-items {{Route::currentRouteName() == 'powdered_products' ? 'top-header-li-active' : ''}}">
                                        <a href="{{route('powdered_products')}}"
                                           class="whiteColor font-weight-600 no-margin">POWDERED JAGGERY</a></li>
                                </ul>

                            </li>
                            <li class="navMenu-items">
                                <a class="whiteColor font-weight-600 {{Route::currentRouteName() == 'benefits' ? 'top-header-li-active' : ''}}" href="{{route('benefits')}}">HEALTH BENEFITS</a>
                            </li>
                            
                            <li class="navMenu-items">   
                                <a class="whiteColor font-weight-600 {{Route::currentRouteName() == 'recipe-corner' ? 'top-header-li-active' : ''}}" href="{{route('recipe-corner')}}">RECIPE CORNER</a>
                            </li>

                            <li class="navMenu-items">
                                <a class="whiteColor font-weight-600 {{Route::currentRouteName() == 'about' ? 'top-header-li-active' : ''}}"
                                   href="{{route('about')}}">ABOUT US</a>
                            </li>

                            <li class="navMenu-items">
                                <a class="whiteColor font-weight-600 {{Route::currentRouteName() == 'contact' ? 'top-header-li-active' : ''}}"
                                   href="{{route('contact')}}">CONTACT US</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>


<div class="mobile-header hide-for-large-up">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <a href="#" class="brand-img">
                    <img src="/img/logo.png" alt=""/>
                </a>
            </li>

            <li class="toggle-topbar menu-icon left"><a href="#"><span></span></a></li>
        </ul>

        <section class="top-bar-section">
            <ul class="right">
                <li class="text-center border-t-wheat"><a
                            class="padding-15 font-weight-600 {{Route::currentRouteName() == 'home' ? 'top-header-li-active' : ''}} "
                            href="{{route('home')}}">HOME</a>
                </li>
                <li class="has-dropdown text-center border-t-wheat">
                    <a class="font-weight-600 lh-50 no-padding">PRODUCTS</a>
                    <ul class="dropdown">
                        <li class="text-center border-t-wheat">
                            <a class="padding-15 font-weight-600 {{Route::currentRouteName() == 'lump_products' ? 'top-header-li-active' : ''}}"
                               href="{{route('lump_products')}}">JAGGERY CUBES</a>
                        </li>
                        <li class="text-center border-t-wheat">
                            <a class="padding-15 font-weight-600 {{Route::currentRouteName() == 'powdered_products' ? 'top-header-li-active' : ''}}"
                               href="{{route('powdered_products')}}">POWDERED JAGGERY</a>
                        </li>

                    </ul>
                </li>
                <li class="text-center border-t-wheat">
                    <a class="padding-15 font-weight-600 {{Route::currentRouteName() == 'benefits' ? 'top-header-li-active' : ''}}" href="{{route('benefits')}}">HEALTH BENEFITS</a>
                </li>
                <li class="text-center border-t-wheat"><a
                            class="padding-15 font-weight-600 {{Route::currentRouteName() == 'recipe-corner' ? 'top-header-li-active' : ''}} "
                            href="{{route('recipe-corner')}}">RECIPE CORNER</a>
                </li>
                <li class="text-center border-t-wheat">
                    <a class="padding-15 font-weight-600 {{Route::currentRouteName() == 'about' ? 'top-header-li-active' : ''}} "
                        href="{{route('about')}}">ABOUT US</a></li>
                <li class="text-center border-t-wheat">
                    <a class="padding-15 font-weight-600 {{Route::currentRouteName() == 'contact' ? 'top-header-li-active' : ''}} "
                        href="{{route('contact')}}">CONTACT US</a></li>
            </ul>
        </section>
    </nav>
</div>











