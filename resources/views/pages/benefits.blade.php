@extends('index')

@section('content')

    <div class="benefits-page bgColor-wheat">
        <div class="container">
            <div class="row">
                <div class="columns small-12">
                    <div class="page_title default-font-family default-title-style">
                        Health Benefits of Jaggery
                    </div>

                    <div class="section_title">
                        Jaggery – A Traditional Indian Sweetener
                    </div>

                    <div class="section_content">
                        <p>
                            Jaggery is the sugarcane based traditional Indian sweetener. At present, 24.5% of the cane
                            produced in India is being utilized for producing jaggery. Jaggery is nutritious and easily
                            available to the rural people. Compared to white sugar, it requires low capital requirement
                            in production and is manufactured at the farmer’s individual units itself. Of the total world
                            production, more than 70% of the jaggery is produced in India. To meet the future
                            sweetener requirement, the scope of jaggery seems to be promising.
                        </p>
                    </div>

                    <div class="section_title">
                        Food Chemistry
                    </div>

                    <div class="section_content">
                        <p>
                            Apart from the nutritional and sweetening aspects of sugars,very little has been studied on
                            their nutraceutical role. The interest in polyphenols, including flavonoids and phenolic
                            acids, has considerably increased in recent years because of their possible role in the
                            prevention of oxidative stress induced diseases such as cardiovascular complications,
                            diabetes, ulcers and cancer
                        </p>

                        <p>
                            Jaggery is the main source of sugar in rural India and has been considered by many
                            Ayurveda practitioners as a wholesome sugar. Indian Ayurvedic medicine considers jaggery to be beneficial in treating throat and lung infections. Sahu and Saxena (1994) have found that jaggery can prevent lung damage from particulate matter such as coal and silica dust in rats. However, there are no reports available in the literature on cytoprotective abilities of jaggery and other sugars and their comparative. Hence, in the present investigation, the protective effect of jaggery in comparison with white, refined and brown sugars on free radical induced damage of NIH 3T3 fibroblasts, erythrocytes and DNA were assessed in addition to 1,1­diphenyl­2­picrylhydrazyl (DPPH) radical scavenging ability and reducing power. Further, the total phenol content and various phenolic acids present in these sugars were also determined.
                        </p>
                    </div>

                    <div class="page_heading">
                        Results
                    </div>

                    <div class="section_title">
                        Total phenol content
                    </div>

                    <div class="section_content">
                        <p>
                            Total phenolic content as quantified by Folin–Ciocalteu method indicated higher total
                            phenolics in jaggery followed by brown, white and refined sugars. Approximately 10­fold
                            higher phenolic content was observed in jaggery (3837 lg GAE/g) compared to brown
                            sugar (372 lg GAE/g). The total phenolic content of white and refined sugars was found to
                            be 31.5 and 26.5 lg GAE/g, respectively.
                        </p>
                    </div>

                    <div class="section_title">
                        Cytoprotective effect of jaggery and other sugars on NIH 3T3 fibroblasts
                    </div>

                    <div class="section_content">
                        <p>
                            The cytoprotective effect of jaggery, white, refined and brown sugars on NIH 3T3
                            fibroblasts indicated 97% protection by jaggery at 4 mg/ml concentration against
                            tert­butyl hydroperoxide induced cell death. However, white and refined sugars showed very less activity and were not statistically significant.
                        </p>
                    </div>

                    <div class="section_title">
                        Antioxidant activities of jaggery and other sugars
                    </div>

                    <div class="section_content">
                        <p>
                            The free radical scavenging ability of sugars as evaluated by DPPH scavenging model
                            system indicated free radical scavenging ability of jaggery, brown, white and refined
                            sugars. Both, jaggery and brown sugars showed free radical scavenging ability. In addition,
                            reducing power of jaggery and other sugars was also evaluated by their ability to reduce
                            ferric chloride and potassium ferricyanide complex. At 20 mg/ml concentration and
                            absorbance unit of 2.66 and 0.248 was observed for jaggery. DNA protective ability of
                            jaggery and other sugars was evaluated on lambda phage DNA oxidation. As evidenced by
                            gel documentation analysis, higher protection (70%) was observed in jaggery treated
                            samples, while 31%, 15% and 18% protection was observed for brown, white and refined
                            sugar treated samples, respectively.
                        </p>
                    </div>

                    <div class="section_title">
                        Discussion
                    </div>

                    <div class="section_content">
                        <p>
                            The different techniques used in cane processing to remove colour and impurities affect the
                            amount of polyphenols in different sugars and this may explain the low phenolic content of
                            white and refined sugars. Further, both jaggery and brown sugar indicated cytoprotective
                            abilities against tert­butyl hydroperoxide and hydrogen peroxide induced oxidative
                            damage of NIH 3T3 fibroblasts and human erythrocytes, respectively. Since sucrose is a
                            non reducing sugar, it is less obvious to have free radical quenching abilities and protect
                            cells from oxidative damage. Hence, the cytoprotective ability may be attributed to the
                            presence of polyphenolic components in jaggery.
                        </p>

                        <p>
                            The antioxidant activity as evaluated by DPPH radical scavenging ability, reducing power
                            and protection to DNA damage induced by hydroxyl radicals also showed the dominant
                            antioxidant potential of the jaggery.
                        </p>
                        <p>
                            In all the experiments, white and refined sugars showed low activity and almost negligible in case of erythrocyte oxidation, reducing power and DPPH radical scavenging assays. But in general, the use of white and refined sugar is more preferred than brown sugar and jaggery because of their colour as well as purity. From our investigation, the presence of cytoprotective and antioxidant activity in jaggery and brown sugar may encourage their use for sweetening as well as for nutraceutical benefits.
                        </p>
                    </div>

                    <div class="section_title">
                        Health Effects of Non­Centrifugal Sugar
                    </div>

                    <div class="section_content">
                        <p>
                            Non­centrifugal sugar (NCS), the technical name of the product obtained by evaporating
                            the water in sugar cane juice, is known by many different names in the world, the most
                            important being unrefined muscovado, whole cane sugar, panela (Latin America), jaggery
                            (South Asia) and kokuto (Japan). Scientific research has been confirming that NCS has
                            multiple health effects but it is still practically outside the current focus on functional foods
                            and nutriceuticals. 46 academic publications have been identified which reports them. The
                            highest frequency is immunological effects (26%), followed by anti­toxicity and
                            cytoprotective effects (22%), anticariogenic effects (15%) and diabetes and hypertension
                            effects (11%). Some of these effects can be traced to the presence of Fe and Cr, and others
                            are suggested to be caused by antioxidants.
                        </p>

                        <p>
                            The antioxidant activity as evaluated by DPPH radical scavenging ability, reducing power
                            and protection to DNA damage induced by hydroxyl radicals also showed the dominant
                            antioxidant potential of the jaggery.
                        </p>
                        <p>
                            In all the experiments, white and refined sugars showed low activity and almost negligible in case of erythrocyte oxidation, reducing power and DPPH radical scavenging assays. But in general, the use of white and refined sugar is more preferred than brown sugar and jaggery because of their colour as well as purity. From our investigation, the presence of cytoprotective and antioxidant activity in jaggery and brown sugar may encourage their use for sweetening as well as for nutraceutical benefits.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="benefits_block">
                        <div class="benefits_block-title">
                            Homa Therapy (The Ancient Science of Healing)
                        </div>
                        <div class="benefits_block-cta">
                            <a target="_blank" href="{{asset('downloads/homa-therapy.pdf')}}" class="bgColor-darkPink pdf-btn whiteColor text-center">Download PDF <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="benefits_block">
                        <div class="benefits_block-title">
                            Scientific study of Vedic Knowledge Agnihotra
                        </div>
                        <div class="benefits_block-cta">
                            <a target="_blank" href="{{asset('downloads/scientific-study-of-vedic-knowledge-agnihotra.pdf')}}" class="bgColor-darkPink pdf-btn whiteColor text-center">Download PDF <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="benefits_block">
                        <div class="benefits_block-title">
                            Jaggery – A Traditional Indian Sweetener
                        </div>
                        <div class="benefits_block-cta">
                            <a target="_blank" href="{{asset('downloads/jaggery.pdf')}}" class="bgColor-darkPink pdf-btn whiteColor text-center">Download PDF <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="benefits_block">
                        <div class="benefits_block-title">
                            Cytoprotective and antioxidant activity studies of jaggery sugar
                        </div>
                        <div class="benefits_block-cta">
                            <a target="_blank" href="{{asset('downloads/cytoprotective-and-antioxidant-activity-studies-of-jaggery-sugar.pdf')}}" class="bgColor-darkPink pdf-btn whiteColor text-center">Download PDF <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="benefits_block">
                        <div class="benefits_block-title">
                            Health Effects of Non-Centrifugal Sugar (NCS): A Review
                        </div>
                        <div class="benefits_block-cta">
                            <a target="_blank" href="{{asset('downloads/health-efects-of-non-centrifugal-sugar-a-review.pdf')}}" class="bgColor-darkPink pdf-btn whiteColor text-center">Download PDF <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection