@extends('index')

@section('content')


    <div class="about-page">
        <section class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-95 s-o-padding-b-30 m-o-padding-t-130 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="default-font-family default-title-style">
                            About us
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- <div class="columns small-12 large-5 medium-12">
                    <div class="aboutCard">
                        <img src="/img/about/first_image.jpg" alt=""/>
                    </div>
                </div> -->
                <div class="columns small-12 large-12 medium-12">
                    <div class="aboutCard-content">
                        <h3 class="darkBrown-color font-weight-600 small-only-margin25">
                            Paawak Foods - A brand known for natural jaggery
                        </h3>

                        <p class="darkBrown-color fontSize1">
                            The company has a dedicated team of professionals who acquire
                            premium quality products from its most trusted engenders,
                            and offer it to the clients in impeccable state with the
                            desired quality. The firm is engaged in supplying jaggery
                            across various components of India.
                        </p>

                        <p class="darkBrown-color fontSize1">
                            The company believes in delivering products with main focus on:
                        </p>
                        <ul>
                            <li>Customer centric approach</li>
                            <li>Inculcating health consciousness</li>
                            <li>Strong base and ethical values</li>
                            <li>Integrity and Innovation</li>
                            <li>Creating strong brand value</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-2 bgColor-white padding-tb-50 s-o-padding-tb-30 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="default-font-family default-title-style">
                            Our Commitments
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns small-12 large-6 medium-6">
                    <div class="row">
                        <div class="columns large-3">
                            <div class="product-img">
                                <i class="fa fa-binoculars" aria-hidden="true" style="font-size: 116px;"></i>
                            </div>
                        </div>

                        <div class="columns large-9">
                            <div class="product-content">
                                <h4 class="margin-b-only darkPink-color s-o-margin-tb-15 m-o-margin-tb-20 font-weight-600">
                                    VISION
                                </h4>

                                <h5 class="font-lato darkBrown-color s-o-margin-b-30">
                                    To be pioneers in health conscious food sector by relentlessly focusing on growing as a business enterprise; with a positive social impact as its backbone of success and continuous growth.
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns small-12 large-6 medium-6">
                    <div class="row">
                        <div class="columns large-3">
                            <div class="product-img">
                                <i class="fa fa-bullseye" aria-hidden="true" style="font-size: 140px;"></i>
                            </div>
                        </div>

                        <div class="columns large-9">
                            <div class="product-content">
                                <h4 class="margin-b-only darkPink-color s-o-margin-tb-15 m-o-margin-tb-20 font-weight-600">
                                    MISSION
                                </h4>

                                <h5 class="font-lato">
                                    To create and promote awareness of the immense benefits jaggery has to offer, and subsequently make people realise the importance of making jaggery as the preferred natural sweetener and an essential part of their daily diet.
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-3 padding-tb-50 s-o-padding-tb-30 m-o-padding-tb-50">
            <div class="bg-img">
                <div class="overlay"></div>
            </div>
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="whiteColor default-font-family default-title-style">
                            Our Team
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="large-12 columns small-12 medium-12">
                    <div class="section-content">
                        <p class="section-content whiteColor margin-b-30">
                            We have a highly adroit and experienced team of professionals
                            from the field of Management, Food processing & Marketing, who
                            consistently work strenuously to maintain the quality of our
                            agro products. They are the most precious assets & the backbone
                            of our company and their main motto is to provide utmost level
                            of gratification to our clients.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns large-4 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <!-- <img src="../img/about/team/t3.jpg" alt=""/> -->

                            <div class="img img-1"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">KAUSHAL DONGRE</h4>

                            <p class="whiteColor s-o-margin-b-30">
                                In a startup, it is important that every person on the core team makes a valuable contribution, and Kaushal, a commerce and law graduate, has been doing it for Paawak Foods since its inception. Kaushal plays a major role in developing the 
                                sales pitch and helps create new and sustainable business relations. He also plays a crucial role in establishing Paawak Foods' brand presence in a nascent market.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
                
                <div class="columns large-4 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <!-- <img src="../img/about/team/t1.jpg" alt=""/> -->

                            <div class="img img-3"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">Mr. M. M. CHITALE</h4>

                            <p class="whiteColor s-o-margin-b-30">
                                Mr. Chitale has done his B.Sc. (Tech) in Food Technology. 
                                He has a 40 years of diversified experience in Product R&D, 
                                Quality Control,  Production,  Projects, Factory & General Management disciplines.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
                <div class="columns large-4 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <!-- <img src="../img/about/team/t4.jpg" alt=""/> -->

                            <div class="img img-4"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">MAHESH DURADI</h4>

                            <p class="whiteColor">
                                He started Homa Organic farming since 2008 with 28 acres of land where he grows sugarcane organically. 
                                With his expertise he manufactures Paawak Natural jaggery powder. Practicing Agnihotra Homa is part of his daily routine.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection