<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_route" content="{{ Route::currentRouteName() }}"/>
    <link rel="icon" type="image/png" href="/img/favicon.png"/>
    <title>Paawak Foods</title>

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
    {{--<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>--}}
    {{--<link href='https://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>--}}
    <link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,300italic,400italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <script src="/js/jquery.js"></script>

    <style>
        /*.location-map{
            width: 100%;
            height: 100%;
            margin-top: 50px;
        }*/

        #map {
            height:500px;
            margin-top: 50px;
        }
    </style>

</head>
<body>

    @include('pages.partials.loader')

    <script>
        $('.dbz-loader').show();
    </script>

    @include('pages.partials.header')

	@yield('content')

    @include('pages.partials.footer')

    <div class="scroll-top fa fa-angle-up"></div>

	<!-- Scripts -->
    <script src="{{ asset('js/all.js') }}"></script>

    <script>
        var dbzLoader = $('.dbz-loader');
        $('body').waitForImages(function () {
            $(window).scrollTop(0);
            dbzLoader.fadeOut();
        }, function (loaded, count, success) {
            dbzLoader.find('.overlay').css('width', 100 - parseInt(((loaded + 1) / count) * 100) + '%');
        }, true);
    </script>

    <script>
        $(document).ready(
            function() {
                $("html body").niceScroll({
                    scrollspeed:60,
                    mousescrollstep:50,
                    cursorwidth:7,
                    cursorborder:'transparent'
                });
            }
        );
    </script>

    <script src="https://maps.googleapis.com/maps/api/js"></script>

    <script>
        function initialize() {
            var mapCanvas = document.getElementById('map');
            var myLatLng = new google.maps.LatLng(19.011093, 73.032629);
            var mapOptions = {
                center: myLatLng,
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            };
            var map = new google.maps.Map(mapCanvas, mapOptions)
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Paawak Foods'
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</body>
</html>
