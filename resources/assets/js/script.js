var _route = '';

$(document).ready(function () {

    _route = $('meta[name=_route]').attr('content');

    $(document).foundation();

    $('.product-slider').slick({
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 8000,
        speed: 300,
        cssEase: 'ease',
        fade: true,
        infinite: true,
        lazyLoad: "ondemand",
        prevArrow: '<span class="nav-prev"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="nav-next"><i class="fa fa-angle-right"></i></span>'
    });


    /* HEADER ANIMATION */
    if ($('.pawak-header').is(":visible")) {
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 100) {
                $(".pawak-header").addClass("stickyHeader");
            }
            else {
                $(".pawak-header").removeClass("stickyHeader");
            }
        })
    }


    $(document).click(function (e) {
        if ($(e.target).is('#products-menu *')) {
            $('.navMenu-items').find('.product-menu-lists').addClass('slide-menu');
        }
        else {
            $('.navMenu-items').find('.product-menu-lists').removeClass('slide-menu');
        }
    });

    $(document).on('mouseover',function (e) {
        if ($(e.target).is('#products-menu *')) {
            $('.navMenu-items').find('.product-menu-lists').addClass('slide-menu');
        }
        else {
            $('.navMenu-items').find('.product-menu-lists').removeClass('slide-menu');
        }
    });

    $.each($('.height-equalizer-wrapper'), function () {
        
        var totalChildEqualizers = $(this).find('.height-equalizer');
        var totalChildrenHeightArr = new Array();
        var indexOfMax;
        $.each(totalChildEqualizers, function () {
            totalChildrenHeightArr.push($(this).outerHeight());
        });
        indexOfMax = totalChildrenHeightArr.indexOf(Math.max.apply(Math, totalChildrenHeightArr));
        totalChildEqualizers.outerHeight($(totalChildEqualizers[indexOfMax]).outerHeight());
    });


    $('#contactpage__form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        form.find('button').prop('disabled', true);

        var message = "<img src='/img/24.gif'>"
        swal({
            title: "Sending Your Message",
            text: message,
            showConfirmButton: true,
            html: true
        });
        
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize(),
            statusCode: {
                200: function (response) {
                    
                    swal({
                        title: "Thank you for Getting in Touch!",
                        text: "We will try to respond as soon as possible, so one of our associate will get back to you within a few hours. Have a great day ahead!",
                        type: "success",
                        confirmButtonText: "Done"
                    });
                    
                    form[0].reset();
                    form.find('button').prop('disabled', false);
                },
                422: function (error) {

                    showContactPageError(error);
                    form.find('button').prop('disabled', false);
                }
            }
        });
    });

    $('.founder-content').readmore({
        speed: 75,
        collapsedHeight: 200,
        lessLink: '<a href="#">Close</a>'
    });
});



var showContactPageError = function (errors) {
    var responseJSON = errors.responseJSON;

    for (var prop in responseJSON) {
        $("[name=" + prop + "]", $("form#contactpage__form")).addClass("invalid");
    }
    for (var prop in responseJSON) {

        $("[name=" + prop + "]", $("form#contactpage__form")).addClass("invalid");
        swal({
            title: "Oops!",
            text: responseJSON[prop][0],
            type: "error",
            confirmButtonText: "Okay"
        });
        return;
    }
};









