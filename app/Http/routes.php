<?php

Route::get('/', ['as' => 'home', 'uses' => 'WebsiteController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'WebsiteController@about']);
Route::get('/recipe/{id}', ['as' => 'recipes', 'uses' => 'WebsiteController@recipes']);

Route::get('/contact', ['as' => 'contact', 'uses' => 'WebsiteController@contact']);
Route::post('/contact/post', ['uses' => 'WebsiteController@storecontactRequest', 'as' => 'contact.post']);

Route::get('/recipe-corner', ['as' => 'recipe-corner', 'uses' => 'WebsiteController@recipeCorner']);
Route::get('/benefits', ['as' => 'benefits', 'uses' => 'WebsiteController@benefits']);

Route::get('/powdered-products', ['as' => 'powdered_products', 'uses' => 'WebsiteController@poweredProducts']);
Route::get('/lump-products', ['as' => 'lump_products', 'uses' => 'WebsiteController@cubedProducts']);

