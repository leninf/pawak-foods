<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App;
use Route;
use Session;

class WebsiteController extends Controller
{

    public function index()
    {
        $recipes = $this->recipeArray();

        return view("pages.home");
    }

    public function about()
    {
        return view("pages.about");
    }

    public function recipeCorner()
    {
        $recipes = array(
            '1' => array(
                'recipe_image' => '20.jpg',
                'recipe_name' => 'Sweetend Rice Flakes'
            ),
            '2' => array(
                'recipe_image' => '1.jpg',
                'recipe_name' => 'Rasgulla Delight'
            ),
            '3' => array(
                'recipe_image' => '2.jpg',
                'recipe_name' => 'Ragi Dills'
            ),
            '4' => array(
                'recipe_image' => '3.jpg',
                'recipe_name' => 'Pinwheel Oats Biscuit'
            ),
            '5' => array(
                'recipe_image' => '4.jpg',
                'recipe_name' => 'Peanut Tongles'
            ),
            '6' => array(
                'recipe_image' => '5.jpg',
                'recipe_name' => 'Peanut Chikki'
            ),
            '7' => array(
                'recipe_image' => '6.jpg',
                'recipe_name' => 'Oats Coconut Poli'
            ),
            '8' => array(
                'recipe_image' => '7.jpg',
                'recipe_name' => 'Nutri Rich Laddoo'
            ),
            '9' => array(
                'recipe_image' => '8.jpg',
                'recipe_name' => 'Nutri Laddoo'
            ),
            '10' => array(
                'recipe_image' => '9.jpg',
                'recipe_name' => 'Ninava'
            ),
            '11' => array(
                'recipe_image' => '10.jpg',
                'recipe_name' => 'Moong Laddoo'
            ),
            '12' => array(
                'recipe_image' => '11.jpg',
                'recipe_name' => 'Moong Laddoo'
            ),
            '13' => array(
                'recipe_image' => '12.jpg',
                'recipe_name' => 'Modak'
            ),
            '14' => array(
                'recipe_image' => '13.jpg',
                'recipe_name' => 'Rice Flake Toffee'
            ),
            '15' => array(
                'recipe_image' => '14.jpg',
                'recipe_name' => 'Spiced Jaggery Tornadoes'
            ),
            '16' => array(
                'recipe_image' => '15.jpg',
                'recipe_name' => 'Spiced Jaggery Tornadoes'
            ),
            '17' => array(
                'recipe_image' => '16.jpg',
                'recipe_name' => 'Sweet Cubes'
            ),
            '18' => array(
                'recipe_image' => '17.jpg',
                'recipe_name' => 'Methi Katori & Karanji'
            ),
            '19' => array(
                'recipe_image' => '18.jpg',
                'recipe_name' => 'Methi Katori'
            ),
            '20' => array(
                'recipe_image' => '19.jpg',
                'recipe_name' => 'Sweeetend Rice Flakes'
            )
        );
        return view("pages.recipe-corner", ['recipes' => $recipes]);
    }

    public function recipes($id)
    {
        $recipes = $this->recipeArray();
        return view("pages.recipe", ['recipe' => $recipes[$id]]);
    }

    public function contact()
    {
        return view("pages.contact");
    }

    public function storecontactRequest(Requests\StoreContactRequest $request)
    {
        $formMessage =  '<strong>From : </strong>' . $request['email'].
                                '<br/><strong>Name : </strong>' . $request['name'] . 
                                '<br/><strong>Phone No : </strong>' . $request['tel'] . 
                                '<br/><strong>Message : </strong>' . $request['message'];
        
        $sendToEmail = 'info@paawakfoods.com';
        
        Mail::raw($formMessage, function ($message) use ($sendToEmail,$request){
                $message->to($sendToEmail)->subject("New Enquiry: Contact Us Page");
                $message->from($request['email'], $request['name']);
            });

        return ['status' => 'success'];
    }


    public function poweredProducts()
    {
        $points = [
            '0' => 'Natural & hygienically made',
            '1' => 'Free from impurities',
            '2' => 'Chemical Free',
            '3' => 'Extremely refined',
            '4' => '100% edible',
            '5' => 'Easy to store & use',
            '6' => 'Non-adulterated',
            '7' => 'Highly nutritious',
            '8' => 'Excellent in taste'
        ];

        return view("pages.powered_products")->with([
            'points' => $points
        ]);
    }

    public function cubedProducts()
    {
        $points = [
            '0' => 'Natural & hygienically made',
            '1' => 'Free from impurities',
            '2' => 'Chemical Free',
            '3' => 'Extremely refined',
            '4' => '100% edible',
            '5' => 'Easy to store & use',
            '6' => 'Non-adulterated',
            '7' => 'Highly nutritious',
            '8' => 'Excellent in taste'
        ];
        return view("pages.cubed_products")->with([
            'points' => $points
        ]);
    }

    function recipeArray()
    {
        return array(
            '1' => array(
                'recipe_image' => '20.jpg',
                'recipe_name' => 'Sweetend Rice Flakes',
                'recipe_desc' => 'Heat ghee in a pan and roast the coconut bits (till it turns golden brown).
                                    Melt jagery with 1/3 cup water and strain it. Add grated coconut to this
                                    and cook on low flame. Cook till the jaggery reduces a bit and the mixture
                                    becomes a semi dry consistency.Add rice flakes and roasted gram dal and
                                    coconut bits to the coconut jaggery mixture. Cook on low flame for 3-4
                                    minutes, stir continuously. Remove from the flame and keep stirring
                                    for 2-3minutes. Add crushed cardamon and mix well. Cover and keep for 20-25
                                    minutes.You can eat it as it is or serve with bananas.',

                'recipe_note' => 'NOTE: Make sure not to cook the jaggery for long after adding rice flakes.
                                    overcooking may harden the rice flakes.If you want you can add 1-2 tbsp of
                                    ghee while cooking jaggery and coconut mixture. Once it is cooled completely
                                    you can store it in air tight container in fridge for a month or so. Keep at
                                    room temperature for 25-30 minutes before serving or microwave for a minute
                                    or so. You can also store it at room temperature in a air tight container
                                    for 2-3 days.',

                'ingredients' => array(
                    '0' => array(
                        'name' => 'Jaggery',
                        'qty' => '250gms'
                    ),
                    '1' => array(
                        'name' => 'Roasted gram dal',
                        'qty' => '1/3 Cup'
                    ),
                    '2' => array(
                        'name' => 'Rice Flakes',
                        'qty' => '250gms'
                    ),
                    '3' => array(
                        'name' => 'Crushed Cardamom',
                        'qty' => '3'
                    ),
                    '4' => array(
                        'name' => 'Coconut bits',
                        'qty' => '1/2 Cup'
                    ),
                    '5' => array(
                        'name' => 'Grated Coconuts',
                        'qty' => '1.5 Cup'
                    ),
                    '6' => array(
                        'name' => 'Ghee',
                        'qty' => '1.5 tbs'
                    )

                )
            ),
            '2' => array(
                'recipe_image' => '1.jpg',
                'recipe_name' => 'Rasgulla Delight',
                'recipe_desc' => 'Boil the water in a vessel. Grate the jiggery in a plate.
                                    Add the melted jaggery in the water and make jaggery syrup. Rub the paneer for
                                    around 10 minutes. Then add maida to it and continue to rub for few minutes.
                                    Makesmall balls of it and then add it to jaggery syrup and let the flame be on
                                    for 10 minutes. Then cool it for some time. Jaggery rasgulla delight are ready to eat .',

                'ingredients' => array(
                    '0' => array(
                        'name' => 'Jaggery',
                        'qty' => '250gms'
                    ),
                    '1' => array(
                        'name' => 'Paneer',
                        'qty' => '250gms'
                    ),
                    '2' => array(
                        'name' => 'Flour(Maida)',
                        'qty' => '10gms'
                    ),
                    '3' => array(
                        'name' => 'Cardomam',
                        'qty' => '2'
                    ),
                    '3' => array(
                        'name' => 'Water',
                        'qty' => ' Cups'
                    )
                )
            ),
            '3' => array(
                'recipe_image' => '2.jpg',
                'recipe_name' => 'Ragi Dills',
                'recipe_desc' => 'Dry heat the ragi flour in a pan. Allow it to cool for some time then add jaggery ,
                                  almonds & walnuts, little amount of coconut (powdered). Add ghee and bind it.
                                  Make heart shapes with help of moulds & roll it on powdered coconut,
                                  then keep it in baking pan. Bake it for 8 mins at 180 degree on convection and
                                  then allow it to cool. Serve once its cooled',

                'ingredients' => array(
                    '0' => array(
                        'name' => 'Ragiflour',
                        'qty' => '30g'
                    ),
                    '1' => array(
                        'name' => 'Jaggery',
                        'qty' => '30g'
                    ),
                    '2' => array(
                        'name' => 'Walnuts / almonds',
                        'qty' => '10g'
                    ),
                    '3' => array(
                        'name' => 'Ghee',
                        'qty' => '30g(2tbps)'
                    ),
                    '4' => array(
                        'name' => 'Powdered Coconut',
                        'qty' => '20g'
                    )
                )
            ),
            '4' => array(
                'recipe_image' => '3.jpg',
                'recipe_name' => 'Pinwheel Oats Biscuit',
                'recipe_desc' => 'Pre-heat the oven to 350o. Cover 2 large cookie sheets with parchment
                                    paper. Set aside. In a bowl, stir together the flour, oats, desiccated
                                    coconut, baking soda, chopped almonds. Make a well in the center and set
                                    aside.In another bowl, beat together jaggery, egg, vanilla, salt using a
                                    hand whisk. When it will be well blended, pour into the well in the flour
                                    mixture and mix well with spatula. Fold in the chopped dates.Using 1tbsp
                                    of dough for each cookie, spoon the dough 2 inches apart onto the prepared
                                    sheets.Line cookie sheet with parchment paper. In a food processor,
                                     blend the oats until they resemble coarse flour. Transfer to a large bowl.
                                     Sift the whole wheat flour; cardamom powder & baking powder. Stir in lemon
                                     zest and salt. In a separate bowl, blend the butter and beans until creamy.
                                     Add jaggery, egg and vanilla essence until combined. This can be done in a
                                     food processor too. Add the bean mixture to spiced oats & flour mixture.
                                     Stir just until combined. Fold in the raisins. On a lightly floured plastic
                                     sheet, roll plain dough into rectangle 16”x9”. Roll chocolate dough of same
                                     size. Place on top, and roll 1/8”thick. Roll up tightly.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Whole wheat flour',
                        'qty' => '30g'
                    ),
                    '1' => array(
                        'name' => 'Oats',
                        'qty' => '60g'
                    ),
                    '2' => array(
                        'name' => 'Rajma',
                        'qty' => '25g'
                    ),
                    '3' => array(
                        'name' => 'Egg',
                        'qty' => '85g (1)'
                    ),
                    '4' => array(
                        'name' => 'Dates',
                        'qty' => '25g'
                    ),
                    '5' => array(
                        'name' => 'Almonds',
                        'qty' => '10g'
                    ),
                    '6' => array(
                        'name' => 'Raisins',
                        'qty' => '10g'
                    ),
                    '7' => array(
                        'name' => 'Dessicated Coconut',
                        'qty' => '15g'
                    ),
                    '8' => array(
                        'name' => 'Jaggery',
                        'qty' => '100g'
                    ),
                    '9' => array(
                        'name' => 'Butter unsalted',
                        'qty' => '50g - 60g'
                    ),
                    '10' => array(
                        'name' => 'Salt',
                        'qty' => '1 pinch'
                    ),
                    '11' => array(
                        'name' => 'Vanilla Essence',
                        'qty' => '1 tsp'
                    )
                )
            ),
            '5' => array(
                'recipe_image' => '4.jpg',
                'recipe_name' => 'Peanut Tangles',
                'recipe_desc' => 'Roast peanuts and jaggery seperately to light golden brown.
                                    Crush the peanuts in mixer grinder. Now mix the jaggery and
                                    peanut together and make a mixture. Now make dough from wheat
                                    flour like we make for paratha. Now make small circles n fill
                                    the mixture in the half of the circles and fold it into a
                                    semicircle and join both the ends of semicircle and make a shape
                                    of hat. Now fry the hats in the hot oil until they are golden brown
                                    in colour. Serve them hot or you can store them in air tight
                                    container for 2 days.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Peanuts',
                        'qty' => '15g'
                    ),
                    '1' => array(
                        'name' => 'Jaggery',
                        'qty' => '20g'
                    ),
                    '2' => array(
                        'name' => 'Wheat Flour',
                        'qty' => '40g'
                    ),
                    '3' => array(
                        'name' => 'Oil',
                        'qty' => 'for frying'
                    )
                )
            ),
            '6' => array(
                'recipe_image' => '5.jpg',
                'recipe_name' => 'Peanut Chikki',
                'recipe_desc' => 'Roast Peanuts until a shade darker. Let them cool down then
                                    remove the skin and ground the peanuts. Dissolve jaggery in
                                    water on medium heat in a saucepan. Bring to boil then let
                                    cook for 3-4 mins until caramalized. Mix peanuts in the jaggery
                                    syrup and mix well while stirring everything quickly. Spread
                                    into a baking sheet lined with parchment paper or plate greased
                                    with ghee.Once it has completely solidified,cut into desired
                                    shapes and serve. Can be stored in an airtight container for upto 3 months.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Jaggery',
                        'qty' => '250g'
                    ),
                    '1' => array(
                        'name' => 'Raw Peanuts',
                        'qty' => '1 Cup'
                    ),
                    '2' => array(
                        'name' => 'Water',
                        'qty' => '1/2 Cup'
                    ),
                    '3' => array(
                        'name' => 'Ghee',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '7' => array(
                'recipe_image' => '6.jpg',
                'recipe_name' => 'Oats Coconut Poli',
                'recipe_desc' => 'In bowl take flour, salt oil & using water prepare soft dough. Coarsely grind
                                    Jaggery first. Now in a mixy jar take oats, coconut cardamom powder,
                                    Jaggery and grind into powder. Now heat a non stick pan with 2 tbsp
                                    ghee and add this ground mixture. Fry a minute add milk stir well& cook.
                                    For 2-3 minute in medium flame & turn off. After 5 minutes apply some
                                    ghee to palms and prepare this mixture into small size balls. Now knead
                                    dough again for 2 minutes & divide into equal size balls. Roll it in puri
                                    size using extra maida & then place oats sweet ball & cover all edges.
                                    Using some more maida roll it in medium size carefully.Prepare all Poli
                                    like this & heat a pan put some ghee & place Poli cook both side evenly.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat flour',
                        'qty' => '1 Cup'
                    ),
                    '1' => array(
                        'name' => 'Oil',
                        'qty' => '1tsp'
                    ),
                    '2' => array(
                        'name' => 'Maida',
                        'qty' => 'for rolling poli'
                    ),
                    '3' => array(
                        'name' => 'Oats',
                        'qty' => '1/2 Cup'
                    ),
                    '4' => array(
                        'name' => 'Coconut Powder',
                        'qty' => '1/2 Cup'
                    ),
                    '5' => array(
                        'name' => 'Cardamom Powder',
                        'qty' => '1tsp'
                    ),
                    '6' => array(
                        'name' => 'Milk',
                        'qty' => '2tsp'
                    )
                )
            ),
            '8' => array(
                'recipe_image' => '7.jpg',
                'recipe_name' => 'Nutri Rich Laddoo',
                'recipe_desc' => 'Chop almonds and cashew nuts into small pieces and roast them
                                    separately till it becomes light brown and crisp. Take black til,
                                    white til, dry grated coconut, flax seeds roast them separately
                                    in a vessel till it becomes crisp.  Melt ghee in a vessel and
                                    add measured amount of wheat flour and gondh gum in same vessel
                                     and cook till it becomes swollen and light brown in colour, remove
                                     all gondh and keep it aside for some time.  Melt jiggery in
                                     different vessel containing ghee and remove from the gas stove.
                                     Add roasted almonds, cashew nuts, black til, white til, dry grated
                                     coconut, flax seeds, gondh gum, dry fig chopped, khuskhus and mawa
                                     together in melted jiggery and mix all the ingredients well with
                                     spoon or hands. Make small ladoo out of the mixture and roll the
                                     ladoo immediately in siloni coconut keep them aside for 5-10 minutes
                                    and serve them.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Almonds',
                        'qty' => '2tsp'
                    ),
                    '1' => array(
                        'name' => 'Cashew nuts',
                        'qty' => '2tsp'
                    ),
                    '2' => array(
                        'name' => 'Black Til',
                        'qty' => '2tsp'
                    ),
                    '3' => array(
                        'name' => 'White Til',
                        'qty' => '1tsp'
                    ),
                    '4' => array(
                        'name' => 'Flax seeds',
                        'qty' => '2tsp'
                    ),
                    '5' => array(
                        'name' => 'Dry grated coconut',
                        'qty' => '1tsp'
                    ),
                    '6' => array(
                        'name' => 'Ghee',
                        'qty' => '2tsp'
                    ),
                    '7' => array(
                        'name' => 'Jaggery',
                        'qty' => '70g'
                    ),
                    '8' => array(
                        'name' => 'Wheat flour',
                        'qty' => '1tsp'
                    ),
                    '9' => array(
                        'name' => 'Dry Figs',
                        'qty' => '2pcs'
                    ),
                    '10' => array(
                        'name' => 'Mawa',
                        'qty' => '2tsp'
                    ),
                    '11' => array(
                        'name' => 'Khas-Khas',
                        'qty' => '2tsp'
                    ),
                    '12' => array(
                        'name' => 'Silicon Coconut',
                        'qty' => 'for garnishing'
                    )
                )
            ),
            '9' => array(
                'recipe_image' => '8.jpg',
                'recipe_name' => 'Nutri Laddoo',
                'recipe_desc' => 'Roast wheat flour and besan flour till golden brown in color.
                                    Fry riceflakes in oil and keep aside. Roast dry coconut and sesame
                                    seeds. Mix wheat flour, besan flour, and jaggery and blend it
                                    in the mixture. Remove the mixture in a place add riceflakes
                                    and crush them with hand and add dry coconut, sesame seeds and
                                    dry fruits, To it add 3 tsp of ghee and mix well and make laddus
                                    of it, Makes 4 laddoos.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat Flour',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Besan Flour',
                        'qty' => '60g'
                    ),
                    '2' => array(
                        'name' => 'Rice Flakes',
                        'qty' => '15g'
                    ),
                    '3' => array(
                        'name' => 'Dry Coconut',
                        'qty' => '10g'
                    ),
                    '4' => array(
                        'name' => 'Sesame seeds',
                        'qty' => '10g'
                    ),
                    '5' => array(
                        'name' => 'Ghee',
                        'qty' => '3-4 tsp'
                    ),
                    '6' => array(
                        'name' => 'Jaggery',
                        'qty' => '6tsp'
                    ),
                    '7' => array(
                        'name' => 'Almonds',
                        'qty' => '10g'
                    ),
                    '8' => array(
                        'name' => 'Raisins',
                        'qty' => '10 - 15'
                    )
                )
            ),
            '10' => array(
                'recipe_image' => '9.jpg',
                'recipe_name' => 'Ninava',
                'recipe_desc' => 'Mix all the ingredients and break all the lumps.
                                    Stir it continuously till it thickens and leaves the side of the vessel.
                                    Now spread it and make it plain in the vessel itself and pour little
                                    ghee from the sides Now close the vessel and let it cook for 10-15 minutes.
                                    Put the flame off and let it cool down.Then cut it into peices and serve',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Coconut Milk',
                        'qty' => '3 Cups'
                    ),
                    '1' => array(
                        'name' => 'Besan',
                        'qty' => '1 Cup'
                    ),
                    '2' => array(
                        'name' => 'Jaggery',
                        'qty' => '1 Cup'
                    ),
                    '3' => array(
                        'name' => 'Elaichi',
                        'qty' => '1/2 Cup'
                    )
                )
            ),
            '11' => array(
                'recipe_image' => '10.jpg',
                'recipe_name' => 'Moong Laddoo',
                'recipe_desc' => 'Roast the dal till the colour get changed grind it finally in the mixture
                                    add jaggery to it then again grind it. Heat ghee in a pan add mixture to it.
                                    Make laddu out off it. Garnish with cashew.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Yellow Moong Dal',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Jaggery',
                        'qty' => '45g'
                    ),
                    '2' => array(
                        'name' => 'Ghee',
                        'qty' => '15g'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '12' => array(
                'recipe_image' => '11.jpg',
                'recipe_name' => 'Moong Laddoo',
                'recipe_desc' => 'Roast the dal till the colour get changed grind it finally in the mixture
                                    add jaggery to it then again grind it. Heat ghee in a pan add mixture to it.
                                    Make laddu out off it. Garnish with cashew.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Yellow Moong Dal',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Jaggery',
                        'qty' => '45g'
                    ),
                    '2' => array(
                        'name' => 'Ghee',
                        'qty' => '15g'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '13' => array(
                'recipe_image' => '12.jpg',
                'recipe_name' => 'Modak',
                'recipe_desc' => 'For the dough :
                                    Boil 13⁄4 cups of water in a deep non-stick pan.
                                    Place the rice flour in a deep bowl and add the boiled water gradually.
                                    Mix well using a spoon in the beginning andthen knead into a soft
                                    and smooth dough.Cover with a lid and keep aside for 10 minutes.

                                    For the filling :
                                    Heat a deep non-stick pan, add the jaggery and cook on a slow flame
                                    for 1 to 2 minutes or till the jaggery melts,while stirring continuously.
                                    Add the coconut, poppy seeds and cardamom powder, mix well and cook on a
                                    slow flame for 4 to 5 minutes or tillall the moisture evaporates and
                                    the mixture thickens. Keep aside to cool slightly.Divide the filling
                                    into 21 equal portions and keep aside.

                                    How to proceed :
                                    Knead the dough once again using 1⁄2 tsp of ghee and keep aside.
                                    Grease a modak mould using very little ghee and close it.
                                    Take a portion of the dough, press it into the cavity of the modak
                                    mould till it is evenly lined on all the sides.Fill the dough cavity
                                    with a portion of the filling.Take a smaller portion of the dough and
                                    spread it evenly at the base of the modak mould so as to seal the filling
                                    Demould the modak from the modak mould.Repeat steps 2 to 7 to make the
                                    remaining 20 modaks.Place a steamer plate in a steamer and place a
                                    banana leaf on it.Moisten all the modaks with little water using your
                                    finger tips.Place 10 modaks on the banana leaf and steam on a medium
                                    flame for 10 minutes.Repeat step 10 to make 11 more modaks in 1 more
                                    batch.Serve warm',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat',
                        'qty' => '1/2 Cup'
                    ),
                    '1' => array(
                        'name' => 'Flour',
                        'qty' => '1/4 Cup'
                    ),
                    '2' => array(
                        'name' => 'Melted Ghee',
                        'qty' => '1/2 Cup'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '14' => array(
                'recipe_image' => '13.jpg',
                'recipe_name' => 'Rice Flake Toffee',
                'recipe_desc' => 'Grind rice flakes in the mixer till it becomes a powder. Put it
                                    in a bowl add pawak jaggery, coconut,nuts, cardamon powder and
                                    ghee mix well and mould into flat round shapes. Heat the tava
                                    add jaggery and caramalize it. Into the melted jaggery place
                                    the moulded ones on both the sides and let it cool',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Rice Flakes',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Jaggery',
                        'qty' => '50g'
                    ),
                    '2' => array(
                        'name' => 'Coconut',
                        'qty' => '50g'
                    ),
                    '3' => array(
                        'name' => 'Nuts',
                        'qty' => '10g'
                    ),
                    '4' => array(
                        'name' => 'Cardamom Powder',
                        'qty' => '5g'
                    ),
                    '5' => array(
                        'name' => 'Ghee',
                        'qty' => '10g'
                    )
                )
            ),
            '15' => array(
                'recipe_image' => '14.jpg',
                'recipe_name' => 'Spice Jaggery Tornadoes',
                'recipe_desc' => 'In a kadai, heat ghee and roast the wheat flour in medium flame
                                    till you get nice aroma. The color of wheat flour will be changed
                                    to golden brown. Keep stirring to avoid getting burnt. Keep the
                                    flame low and add grated jaggery to this and mix well. After all
                                    the jaggery is combined well with the flour. Add milk and stir.
                                    Switch off the flame immediately. It takes a minute only. Put
                                    this mixture into the ghee greased plate. After 2-3 minutes,
                                    mark the shapes using a knife. Leave it aside for 10 minutes.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat',
                        'qty' => '1/2 Cup'
                    ),
                    '1' => array(
                        'name' => 'Flour',
                        'qty' => '1/4 Cup'
                    ),
                    '2' => array(
                        'name' => 'Melted Ghee',
                        'qty' => '1/2 Cup'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '16' => array(
                'recipe_image' => '15.jpg',
                'recipe_name' => 'Spice Jaggery Tornadoes',
                'recipe_desc' => 'In a kadai, heat ghee and roast the wheat flour in medium flame
                                    till you get nice aroma. The color of wheat flour will be changed
                                    to golden brown. Keep stirring to avoid getting burnt. Keep the
                                    flame low and add grated jaggery to this and mix well. After all
                                    the jaggery is combined well with the flour. Add milk and stir.
                                    Switch off the flame immediately. It takes a minute only. Put
                                    this mixture into the ghee greased plate. After 2-3 minutes,
                                    mark the shapes using a knife. Leave it aside for 10 minutes.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat',
                        'qty' => '1/2 Cup'
                    ),
                    '1' => array(
                        'name' => 'Flour',
                        'qty' => '1/4 Cup'
                    ),
                    '2' => array(
                        'name' => 'Melted Ghee',
                        'qty' => '1/2 Cup'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '17' => array(
                'recipe_image' => '16.jpg',
                'recipe_name' => 'Sweet Cubes',
                'recipe_desc' => 'Take 60gm of wheat flour and add a pinch of salt to it.
                                    Take 20gm of organic jaggery and melt it in minimum quantity
                                    of water so that the jaggery melts. Add this melted jaggery to
                                    the wheat flour and make a dough. For the stuffing -- take 20gm
                                    of organic jaggery and add 20gm of dry coconut cook them at low
                                    flame flame for 5 minute. Make small balls of dough and add
                                    stuffing to it and give it shapes of small cubes. Fry these
                                    cubes till golden brown and serve.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat',
                        'qty' => '1/2 Cup'
                    ),
                    '1' => array(
                        'name' => 'Flour',
                        'qty' => '1/4 Cup'
                    ),
                    '2' => array(
                        'name' => 'Melted Ghee',
                        'qty' => '1/2 Cup'
                    ),
                    '3' => array(
                        'name' => 'Milk',
                        'qty' => '1/4 Cup'
                    )
                )
            ),
            '18' => array(
                'recipe_image' => '17.jpg',
                'recipe_name' => 'Meethi Katori & Karanji',
                'recipe_desc' => 'Melt the powdered jaggery in 1⁄2 cup water in a kadhai.
                                    Take 60gms wheat flour in a big plate, add ghee to it.
                                    Add the melted jaggery at intervals with spoon to make soft
                                    dough. Divide the dough into 5 parts & make small rotis out
                                    of it. Heat oil in a kadhai. Take an alluminium design katori
                                    & place 1 roti behind it & press along the shape of the katori.
                                    Make holes at back of the katori using a fork. Put the katori
                                    in the heated oil & let it cook for some seconds. Then gradually
                                    remove the katori using a pakkad.Repeat the same with another roti.
                                    After the katoris are formed place some strips of coconut & a
                                    little jaggery mixed with desiccated coconut. Place a small
                                    amount of desiccated coconut & jaggery powder in the centre
                                    of the remaining rotis & turn it half into a semi circle so
                                    that the filing remains inside. Press the corner parts by bringing
                                    the left side a little towards right. Till the end of the semi
                                    circle.Fry them in the heated oil.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat Flour',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Ghee',
                        'qty' => '1tsp'
                    ),
                    '2' => array(
                        'name' => 'Salt',
                        'qty' => 'for taste'
                    ),
                    '3' => array(
                        'name' => 'Jaggery Powder',
                        'qty' => '2tbsp (melted in 1/2 cup water)'
                    ),
                    '3' => array(
                        'name' => 'Dessicated Coconut',
                        'qty' => '1tbsp'
                    ),
                    '3' => array(
                        'name' => 'Oil',
                        'qty' => 'for frying'
                    )
                )
            ),
            '19' => array(
                'recipe_image' => '18.jpg',
                'recipe_name' => 'Meethi Katori',
                'recipe_desc' => 'Melt the powdered jaggery in 1⁄2 cup water in a kadhai.
                                    Take 60gms wheat flour in a big plate, add ghee to it.
                                    Add the melted jaggery at intervals with spoon to make soft
                                    dough. Divide the dough into 5 parts & make small rotis out
                                    of it. Heat oil in a kadhai. Take an alluminium design katori
                                    & place 1 roti behind it & press along the shape of the katori.
                                    Make holes at back of the katori using a fork. Put the katori
                                    in the heated oil & let it cook for some seconds. Then gradually
                                    remove the katori using a pakkad.Repeat the same with another roti.
                                    After the katoris are formed place some strips of coconut & a
                                    little jaggery mixed with desiccated coconut. Place a small
                                    amount of desiccated coconut & jaggery powder in the centre
                                    of the remaining rotis & turn it half into a semi circle so
                                    that the filing remains inside. Press the corner parts by bringing
                                    the left side a little towards right. Till the end of the semi
                                    circle.Fry them in the heated oil.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Wheat Flour',
                        'qty' => '60g'
                    ),
                    '1' => array(
                        'name' => 'Ghee',
                        'qty' => '1tsp'
                    ),
                    '2' => array(
                        'name' => 'Salt',
                        'qty' => 'for taste'
                    ),
                    '3' => array(
                        'name' => 'Jaggery Powder',
                        'qty' => '2tbsp (melted in 1/2 cup water)'
                    ),
                    '3' => array(
                        'name' => 'Dessicated Coconut',
                        'qty' => '1tbsp'
                    ),
                    '3' => array(
                        'name' => 'Oil',
                        'qty' => 'for frying'
                    )
                )
            ),
            '20' => array(
                'recipe_image' => '19.jpg',
                'recipe_name' => 'Sweetend Rice Flakes',
                'recipe_desc' => 'Heat ghee in a pan and roast the coconut bits (till it turns golden brown).
                                    Melt jagery with 1/3 cup water and strain it. Add grated coconut to this
                                    and cook on low flame. Cook till the jaggery reduces a bit and the mixture
                                    becomes a semi dry consistency.Add rice flakes and roasted gram dal and
                                    coconut bits to the coconut jaggery mixture. Cook on low flame for 3-4
                                    minutes, stir continuously. Remove from the flame and keep stirring
                                    for 2-3minutes. Add crushed cardamon and mix well. Cover and keep for 20-25
                                    minutes.You can eat it as it is or serve with bananas.',
                'ingredients' => array(
                    '0' => array(
                        'name' => 'Jaggery',
                        'qty' => '250gms'
                    ),
                    '1' => array(
                        'name' => 'Roasted gram dal',
                        'qty' => '1/3 Cup'
                    ),
                    '2' => array(
                        'name' => 'Rice Flakes',
                        'qty' => '250gms'
                    ),
                    '3' => array(
                        'name' => 'Crushed Cardamom',
                        'qty' => '3'
                    ),
                    '4' => array(
                        'name' => 'Coconut bits',
                        'qty' => '1/2 Cup'
                    ),
                    '5' => array(
                        'name' => 'Grated Coconuts',
                        'qty' => '1.5 Cup'
                    ),
                    '6' => array(
                        'name' => 'Ghee',
                        'qty' => '1.5 tbs'
                    )

                )
            )
        );
    }

    public function benefits()
    {
        return view('pages.benefits');
    }
}
